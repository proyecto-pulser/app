PulSer - A medication reminder
===============================

Based on RxDroid by Joseph C. Lehner

PulSer is a medication reminder app. Apart from reminding you to take 
your meds, it will also keep track of your pill count, warning you to get a 
refill in time.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/at.jclehner.rxdroid/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=at.jclehner.rxdroid)

This app is free and open source, contains no ads, and requires only very 
limited permissions.
